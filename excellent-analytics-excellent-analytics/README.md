Excellent Analytics
======================

Excellent Analytics is an addin for Microsoft Excel that makes it possible to fetch data from Google Analytics to a spreadsheet.

Contact us if you would like to contribute to this project. You must use your business e-mail address and identify yourself by full name and contact details. Please also state how often you plan on contributing code. We only want to spend time on administering users who will actually contribute.
