﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;

namespace GA_API
{
    class GoogleProfileData
    {
        public GoogleProfileData()
        {
        }

        public GoogleProfileData(GoogleProfileInfo sfdc)
        {
            // A reference of the business object class
        }

        //standard dataset function that finds and 
        //return the detail of a customer in a dataset
        public DataSet GetGoogleProfiles()
        {
            //SqlConnection con = new SqlConnection("Persist Security Info=False;Initial Catalog=Mosaic;Data Source=(local);UID=Mosaic;PWD=griz2tb");
            SqlConnection con = new SqlConnection(Globals.SQLCnnStr);

            con.Open();            
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable(); 
            DataSet ds = new DataSet();

            try
            {
                cmd = new SqlCommand("MosaicGetGoogleProfiles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.ExecuteReader();
                da.SelectCommand = cmd;
                //da.Fill(dt);
                //ds = dt.DataSet;
                da.Fill(ds);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e);
            }
            finally
            {
                cmd.Dispose();
                con.Close(); 
            }
            return ds;
        }
    }
}
