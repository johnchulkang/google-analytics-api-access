﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Xml;
using System.Xml.Xsl;
using System.Threading;
using System.IO;
using System.Web;
//using System.Web.SessionState;
using System.Reflection;

namespace GA_API
{
    class GASvc
    {
        DataSet ds = null;
        GoogleProfileData gaProfileData = new GoogleProfileData();

        public MySortableBindingList<GoogleProfileInfo> GetProfiles()
        {
            try
            {
                MySortableBindingList<GoogleProfileInfo> googleProfiles = new MySortableBindingList<GoogleProfileInfo>();

                int row = 0;

                try
                {
                    ds = gaProfileData.GetGoogleProfiles();
                    //da.Fill(ds, "VendorInfo");

                    //--- Loop through the related OrderData for the Current Customer
                    for (row = 0; row < ds.Tables[0].Rows.Count; )
                    {
                        //--- Create an Order Object Instance
                        GoogleProfileInfo profile = new GoogleProfileInfo();

                        //--- Map the Relational data to Object properties for Order
                        ORM.RelationalToObject(profile, ds.Tables[0].Rows[row]);

                        //--- add to collection
                        googleProfiles.Add(profile);
                        row += 1;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }

                return googleProfiles;
            }
            catch (Exception ex)
            {
                Globals.LogErrors(ex);
                return null;
            }
        }
        
        public static DataSet ExecuteCmd(SqlCommand cmd)
        {
            DataSet dataset = null;

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.ExecuteNonQuery();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            // Fill the DataSet using default values for DataTable names, etc
            da.Fill(dataset);

            return dataset;
        }
    }

    public class MySortableBindingList<T> : BindingList<T>
    {

        // reference to the list provided at the time of instantiation
        List<T> originalList;
        ListSortDirection sortDirection;
        PropertyDescriptor sortProperty;

        // function that refereshes the contents
        // of the base classes collection of elements
        Action<MySortableBindingList<T>, List<T>>
                        populateBaseList = (a, b) => a.ResetItems(b);

        // a cache of functions that perform the sorting
        // for a given type, property, and sort direction
        static Dictionary<string, Func<List<T>, IEnumerable<T>>>
            cachedOrderByExpressions = new Dictionary<string, Func<List<T>,
                                                        IEnumerable<T>>>();

        public MySortableBindingList()
        {
            originalList = new List<T>();
        }

        public MySortableBindingList(IEnumerable<T> enumerable)
        {
            originalList = enumerable.ToList();
            populateBaseList(this, originalList);
        }

        public MySortableBindingList(List<T> list)
        {
            originalList = list;
            populateBaseList(this, originalList);
        }

        public event ListChangedEventHandler ListChanged;

        protected override void ApplySortCore(PropertyDescriptor prop,
                                ListSortDirection direction)
        {
            /*
                Look for an appropriate sort method in the cache if not found .
                Call CreateOrderByMethod to create one. 
                Apply it to the original list.
                Notify any bound controls that the sort has been applied.
                */

            sortProperty = prop;

            var orderByMethodName = sortDirection ==
                ListSortDirection.Ascending ? "OrderBy" : "OrderByDescending";
            var cacheKey = typeof(T).GUID + prop.Name + orderByMethodName;

            if (!cachedOrderByExpressions.ContainsKey(cacheKey))
            {
                CreateOrderByMethod(prop, orderByMethodName, cacheKey);
            }

            ResetItems(cachedOrderByExpressions[cacheKey](originalList).ToList());
            ResetBindings();
            sortDirection = sortDirection == ListSortDirection.Ascending ?
                            ListSortDirection.Descending : ListSortDirection.Ascending;
        }


        private void CreateOrderByMethod(PropertyDescriptor prop,
                        string orderByMethodName, string cacheKey)
        {

            /*
                Create a generic method implementation for IEnumerable<T>.
                Cache it.
            */

            var sourceParameter = Expression.Parameter(typeof(List<T>), "source");
            var lambdaParameter = Expression.Parameter(typeof(T), "lambdaParameter");
            var accesedMember = typeof(T).GetProperty(prop.Name);
            var propertySelectorLambda =
                Expression.Lambda(Expression.MakeMemberAccess(lambdaParameter,
                                    accesedMember), lambdaParameter);
            var orderByMethod = typeof(Enumerable).GetMethods()
                                            .Where(a => a.Name == orderByMethodName &&
                                                        a.GetParameters().Length == 2)
                                            .Single()
                                            .MakeGenericMethod(typeof(T), prop.PropertyType);

            var orderByExpression = Expression.Lambda<Func<List<T>, IEnumerable<T>>>(
                                        Expression.Call(orderByMethod,
                                                new Expression[] { sourceParameter, 
                                                            propertySelectorLambda }),
                                                sourceParameter);

            cachedOrderByExpressions.Add(cacheKey, orderByExpression.Compile());
        }

        protected override void RemoveSortCore()
        {
            ResetItems(originalList);
        }

        private void ResetItems(List<T> items)
        {
            base.RaiseListChangedEvents = false;
            base.ClearItems();

            for (int i = 0; i <= items.Count - 1; i++)
            {
                base.InsertItem(i, items[i]);
            }

            base.RaiseListChangedEvents = true;
            base.ResetBindings();
        }

        protected override bool SupportsSortingCore
        {
            get
            {
                // indeed we do
                return true;
            }
        }

        protected override ListSortDirection SortDirectionCore
        {
            get
            {
                return sortDirection;
            }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get
            {
                return sortProperty;
            }
        }

        protected override void OnListChanged(ListChangedEventArgs e)
        {
            originalList = base.Items.ToList();
            base.OnListChanged(e);
        }

        private IComparer<T> comparer;

        public void AddSorted(T item)
        {
            if (base.Items.IsReadOnly)
            {
                throw new NotSupportedException("CollectionReadOnly");
            }
            int index = Array.BinarySearch<T>(base.Items.ToArray(), 0, base.Items.Count, item, this.comparer);
            if (index >= 0)
            {
                base.Insert(index, item);
            }
            else if (~index == base.Items.Count)
            {
                base.Add(item);
            }
            else
            {
                base.Insert(~index, item);
            }
        }

        public void Sort(PropertyDescriptor prop, ListSortDirection direction)
        {
            ApplySortCore(prop, direction);
        }

    }

    public class PropertyComparer<T> : System.Collections.Generic.IComparer<T>
    {
        private PropertyDescriptor _property;
        private ListSortDirection _direction;

        public PropertyComparer(PropertyDescriptor property,
            ListSortDirection direction)
        {
            _property = property;
            _direction = direction;
        }

        #region IComparer<T>

        public int Compare(T xWord, T yWord)
        {
            // Get property values
            object xValue = GetPropertyValue(xWord, _property.Name);
            object yValue = GetPropertyValue(yWord, _property.Name);

            // Determine sort order
            if (_direction == ListSortDirection.Ascending)
            {
                return CompareAscending(xValue, yValue);
            }
            else
            {
                return CompareDescending(xValue, yValue);
            }
        }

        public bool Equals(T xWord, T yWord)
        {
            return xWord.Equals(yWord);
        }

        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }

        #endregion

        // Compare two property values of any type
        private int CompareAscending(object x, object y)
        {
            int result;

            // If values implement IComparer
            if (x is IComparable)
            {
                result = ((IComparable)x).CompareTo(y);
            }
            // If values don't implement IComparer but are equivalent
            else if (x.Equals(y))
            {
                result = 0;
            }
            // Values don't implement IComparer and are not equivalent,
            // so compare as typed values
            else result = ((IComparable)x).CompareTo(y);

            // Return result
            return result;
        }

        private int CompareDescending(object x, object y)
        {
            // Return result adjusted for ascending or descending sort order ie
            // multiplied by 1 for ascending or -1 for descending
            return -CompareAscending(x, y);
        }

        private object GetPropertyValue(T value, string property)
        {
            // Get property
            PropertyInfo propertyInfo = value.GetType().GetProperty(property);

            // Return value
            return propertyInfo.GetValue(value, null);
        }
    }

}
