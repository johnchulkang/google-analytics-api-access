﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace GA_API
{
    public static class Globals
    {
        public static String SQLCnnStr = Properties.Settings.Default.SQLConnStr;

        public static bool ValidateNumbers(char chr, object sender)
        {
            if (!char.IsControl(chr)
                && !char.IsDigit(chr)
                && chr != '-'
                && chr != '.')
            {
                return true;
            }

            // only allow one decimal point
            if (chr == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                return true;
            }
            return false;
        }

        public static void transpose(int[][] a, int[][] b, int width, int height)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    b[j][i] = a[i][j];
                }
            }
        }

        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

        public static Type GetListType(object someList)
        {
            if (someList == null)
                throw new ArgumentNullException("someList");

            var type = someList.GetType();

            if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(List<>))
                throw new ArgumentException("someList", "Type must be List<>, but was " + type.FullName);

            return type.GetGenericArguments()[0];
        }

        public static void LogErrors(Exception ex)
        {
            string filePath = @"C:\PTA_Error.txt";

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
    }
}
