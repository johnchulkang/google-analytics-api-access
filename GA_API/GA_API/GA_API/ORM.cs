﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;

namespace GA_API
{
    public class ORM
    {

        public static void RelationalToObject(object p_obj, DataRow p_dataRow)
        {
            //--- Get Object Properties
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(p_obj);

            //--- Apply OR-Mapping

            foreach (DataColumn column in p_dataRow.Table.Columns)
            {


                for (int propertyIndex = 0; propertyIndex < props.Count; propertyIndex++)
                {
                    PropertyDescriptor prop;
                    try
                    {
                        prop = props[propertyIndex];
                    }
                    catch
                    {
                        continue;
                    }


                    //--- Omit IListTypes (childcollections)
                    if (prop.PropertyType.GetInterface("IList") == null && !prop.IsReadOnly)
                    {
                        if (prop.Name.Trim().ToLower() == column.ColumnName.Trim().ToLower())
                        {
                            if (p_dataRow[column] != DBNull.Value)
                            {
                                prop.SetValue(p_obj, p_dataRow[column]);
                            }
                        }
                    }
                }
            }

        }

        public static void ObjectToRelational(object p_obj, DataRow p_dataRow)
        {
            //--- Get Object Properties
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(p_obj);

            //--- Loop trough the object properties
            for (int propertyIndex = 0; propertyIndex < props.Count; propertyIndex++)
            {
                //--- Get the Property
                PropertyDescriptor prop = props[propertyIndex];

                //--- Map data to Appropriate Row Column, omiting ChildCollections
                if (prop.PropertyType.GetInterface("IList") == null)
                {
                    try
                    {
                        try
                        {
                            if (prop.Name.Trim().ToLower() == p_dataRow.Table.Columns[prop.Name].ColumnName.Trim().ToLower())
                            {
                                p_dataRow[prop.Name] = prop.GetValue(p_obj);
                            }
                        }
                        catch //--- Ignore non Row Related properties
                        {
                            continue;
                        }


                    }
                    catch (NotImplementedException ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}