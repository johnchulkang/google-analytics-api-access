﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using System.Globalization;
using Google.Apis.Analytics.v3;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Services;
using System.IO;
using Google.Apis.Analytics.v3.Data;

namespace GA_API
{
    class Program
    {
        static string keyFilePath = @"C:\Projects\GoogleAnalytics\GrizGAIntegration-3096fbc1d02b.p12";    // Downloaded from https://console.developers.google.com
        //static string serviceAccountEmail = "265295597976-qfo4aj7rn331veb232fgguis1mvm7gg6@developer.gserviceaccount.com";  // found https://console.developers.google.com
        static string serviceAccountEmail = "gaintegration@supple-outlet-122618.iam.gserviceaccount.com";  // found https://console.developers.google.com
        static X509Certificate2 certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
        static string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data
        static ServiceAccountCredential credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail) { Scopes = scopes }.FromCertificate(certificate));
        static AnalyticsService service = new AnalyticsService(new BaseClientService.Initializer() { HttpClientInitializer = credential, ApplicationName = "My Project", });

        static void Main(string[] args)
        {
            DateTime startDate, endDate;
            GASvc GAService = new GASvc();
            MySortableBindingList<GoogleProfileInfo> gaProfiles = new MySortableBindingList<GoogleProfileInfo>();
    
            long rowCount = 1;

            //loading the Key file
            //var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            //var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            //{
            //    Scopes = scopes
            //}.FromCertificate(certificate));

            //var service = new AnalyticsService(new BaseClientService.Initializer()
            //{
            //    HttpClientInitializer = credential,
            //    ApplicationName = "API Project",
            //});

            //OutputResults();

            // start data output to file (delete if same already exists)
            String filename = "GRZ_WEBSITE_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".txt";
            if (File.Exists(@filename))
            {
                File.Delete(@filename);
            }

            // initiate write and add header row
            InitiateTracer(filename);
            Trace.WriteLine("AGENCY_ID|SOURCE_DMS_ID|ORG_ID|ORG_LEVEL|DATE|CHANNELGROUPING|DEVICECATEGORY|USERS|NEWUSERS|PERCENTNEWSESSIONS|SESSIONS|PAGEVIEWS|BOUNCERATE|SESSIONDURATION|AVGSESSIONDURATION|TRANSACTIONS|TRANSACTIONREVENUE|RECORD_NUM|PROCESSING_TIMESTAMP");

            gaProfiles = GAService.GetProfiles();

            startDate = new DateTime(2017, 1, 1);
            endDate = new DateTime(2017, 2, 28);
            //startDate = DateTime.Today.AddDays(-1);
            //endDate = DateTime.Today.AddDays(-1);

            foreach (GoogleProfileInfo profile in gaProfiles)
            {
                rowCount += GetGoogleData(profile.Google_Profile_ID__c, startDate, endDate, profile.DMSSourceID__c, profile.Area_ID__c, profile.Unit_Level__c, rowCount);
            }

            //GetAccountSummaries();
            
        }

        private static void SampleTest()
        {
            int totalResults, rowcnt=0;
            string NextLink;

            GoogleAnalyticsAPI gapi = new GoogleAnalyticsAPI(keyFilePath, serviceAccountEmail);

            //FRM:22364211 ARM:ga:43934800 PRM:ga:93160202 

            //var request = service.Data.Ga.Get("ga:22364211", "2015-10-01", "2016-08-31", "ga:newUsers,ga:percentNewSessions,ga:sessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession");
            var request = service.Data.Ga.Get("ga:22364211", "2015-10-01", "2016-08-31", "ga:newUsers,ga:percentNewSessions,ga:sessions,ga:bounceRate");
            //Specify some addition query parameters
            //request.Key = keyFilePath;
            request.Dimensions = "ga:isoYear,ga:month";
            //request.Segment = "gaid::-1";
            request.MaxResults = 10000;

            List<IList<string>> result = new List<IList<string>>();
            do {
                try
                {
                    GaData DataList = request.Execute();  // Make the request
                    result.AddRange(DataList.Rows);       // store the Data to return later
                    // hack to get next link stuff
                    totalResults = (!DataList.TotalResults.HasValue) ? 0 : Int32.Parse(DataList.TotalResults.ToString());
                    rowcnt = rowcnt + DataList.Rows.Count;
                    NextLink = DataList.NextLink;                       
                    request.StartIndex = rowcnt + 1 ;
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                    totalResults = 0;
                 }
            } while (request.StartIndex <= totalResults);

            //InitiateTracer("GA_API_Trace_SAMPLE.txt");

            //Execute and fetch the results of our query
            Google.Apis.Analytics.v3.Data.GaData d = request.Execute();

            //InitiateTracer("GA_API_Trace_SAMPLE.txt");

            try
            {
                //Trace.WriteLine(d.Rows);
                OutputResults2(d);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }
        }

        private static long GetGoogleData(String profileId, DateTime startDate, DateTime endDate, String dmsSource, String orgID, decimal orgLevel, long startingRow)
        {
            String rowLine, rowConstant;
            GoogleAnalyticsAPI gapi = new GoogleAnalyticsAPI(keyFilePath, serviceAccountEmail);

            var request = service.Data.Ga.Get("ga:" + profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:users,ga:newUsers,ga:percentNewSessions,ga:sessions,ga:pageviews,ga:bounceRate,ga:sessionDuration,ga:avgSessionDuration,ga:transactions,ga:transactionRevenue");

            //Specify some addition query parameters
            //request.Key = keyFilePath;
            request.Dimensions = "ga:date,ga:channelGrouping,ga:deviceCategory";
            //request.Segment = "gaid::-1";
            request.MaxResults = 10000;

            var report = request.Execute();
            var rows = report.Rows;
            var cols = report.ColumnHeaders;
            var newVisitors = rows[0];
            var returnVisitors = rows[1];
            int i;

            //declare data for first three columns
            rowConstant = "GRZ|" + dmsSource + "|" + orgID + "|" + orgLevel.ToString();

            // write data rows
            for (i = 0; i < rows.Count; i++)
            {
                rowLine = rowConstant ;
                for (int j = 0; j < cols.Count; j++)
                {
                    switch (cols[j].Name)
                    {
                        case "ga:date":
                            {
                                rowLine += "|" + DateTime.ParseExact(rows[i][j], "yyyyMMdd", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy"); ;
                                break;
                            }

                        case "ga:percentNewSessions":
                        case "ga:bounceRate":
                        case "ga:avgSessionDuration":
                            {
                                rowLine += "|" + String.Format("{0:0.00}", Math.Round(Convert.ToDecimal(rows[i][j]),2));
                                break;
                            }

                        case "ga:transactionRevenue":
                            {
                                rowLine += "|" + String.Format("{0:0.00}", Convert.ToDecimal(rows[i][j])); ;
                                break;
                            }

                        default:
                            {
                                rowLine += "|" + rows[i][j];
                                break;
                            }
                    }
                }
                //rowLine += "|" + (i + 1).ToString() + "|" + DateTime.Today.ToString("MM/dd/yyyy HH:mm:ss");
                rowLine += "|" + startingRow.ToString() + "|" + String.Format("{0:MM/dd/yyyy HH:mm:ss}", DateTime.Now);
                Trace.WriteLine(rowLine);
                startingRow += 1;
            }

            return i;
        }

        private static void OutputResults()
        {
            MetadataResource.ColumnsResource.ListRequest request = service.Metadata.Columns.List("ga");
            Columns result = request.Execute();

            InitiateTracer();
            foreach (Column cols in result.Items)
            {
                Trace.WriteLine("ApiName :" + cols.Id);
                Trace.WriteLine("UIName : " + cols.Attributes["uiName"]);
                Trace.WriteLine("Group : " + cols.Attributes["group"]);
                Trace.WriteLine("Type : " + cols.Attributes["type"]);
                Trace.WriteLine("Description: " + cols.Attributes["description"]);
                Trace.WriteLine("");
            }
        }


        private static void OutputResults2(Google.Apis.Analytics.v3.Data.GaData d)
        {
            var rows = d.Rows;
            var newUsers = rows[0];
            var percentNewSessions = rows[1];
            var sessions = rows[2];
            var bounceRate = rows[3];
            var avgSessionDuration = rows[4];
            var pageviewsPerSession = rows[5];

            InitiateTracer();
            //foreach (Column cols in d)
            //{
            //    Trace.WriteLine("ApiName :" + cols.Id);
            //    Trace.WriteLine("UIName : " + cols.Attributes["uiName"]);
            //    Trace.WriteLine("Group : " + cols.Attributes["group"]);
            //    Trace.WriteLine("Type : " + cols.Attributes["type"]);
            //    Trace.WriteLine("Description: " + cols.Attributes["description"]);
            //    Trace.WriteLine("");
            //}
        }

        private static void GetAccountSummaries()
        {
            GoogleAnalyticsAPI gapi = new GoogleAnalyticsAPI(keyFilePath, serviceAccountEmail);
            ManagementResource.AccountSummariesResource.ListRequest list = gapi.Service.Management.AccountSummaries.List();
            list.MaxResults = 1000;  // Maximum number of Account Summaries to return per request. 

            AccountSummaries feed = list.Execute();
            List<AccountSummary> allRows = new List<AccountSummary>();

            //// Loop through until we arrive at an empty page
            while (feed.Items != null)
            {
                allRows.AddRange(feed.Items);

                // We will know we are on the last page when the next page token is
                // null.
                // If this is the case, break.
                if (feed.NextLink == null)
                {
                    break;
                }

                // Prepare the next page of results             
                list.StartIndex = feed.StartIndex + list.MaxResults;
                // Execute and process the next page request
                feed = list.Execute();
            }

            feed.Items = allRows;  // feed.Items not contains all of the rows even if there are more then 1000         

            InitiateTracer("GA_API_Trace_GrizAccounts.txt");

            //Get account summary and display them.
            foreach (AccountSummary account in feed.Items)
            {
                // Account
                //Console.WriteLine("Account: " + account.Name + "(" + account.Id + ")");
                //Trace.Write("Account: " + account.Name + "(" + account.Id + ")");
                foreach (WebPropertySummary wp in account.WebProperties)
                {
                    // Web Properties within that account
                    //Trace.Write("\tWeb Property: " + wp.Name + "(" + wp.Id + ")");

                    //Don't forget to check its not null. Believe it or not it could be.
                    if (wp.Profiles != null)
                    {
                        foreach (ProfileSummary profile in wp.Profiles)
                        {
                            // Profiles with in that web property.
                            //Trace.WriteLine("\t\tProfile: " + profile.Name + "(" + profile.Id + ")");
                            Trace.WriteLine(account.Name + "\t" + account.Id + "\t" + wp.Name + "\t" + wp.Id + "\t" + profile.Name + "\t" + profile.Id);
                        }
                    }
                }
            }        
        }


        /// <summary>  
        /// Initiates a Tracer which will print to both  
        /// the Console and to a log file, log.txt  
        /// </summary>  
        private static void InitiateTracer(string fileName = "GA_API_Trace_GRIZ.txt")
        {
            Trace.Listeners.Clear();
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            var twtl = new TextWriterTraceListener(fileName)
            {
                Name = "TextLogger",
                TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime
            };
            var ctl = new ConsoleTraceListener(false) { TraceOutputOptions = TraceOptions.DateTime };
            Trace.Listeners.Add(twtl);
            Trace.Listeners.Add(ctl);
            Trace.AutoFlush = true;
        }

        public class GoogleAnalyticsAPI
        {
            public AnalyticsService Service { get; set; }

            public GoogleAnalyticsAPI(string keyPath, string accountEmailAddress)
            {
                var certificate = new X509Certificate2(keyPath, "notasecret", X509KeyStorageFlags.Exportable);

                var credentials = new ServiceAccountCredential(
                   new ServiceAccountCredential.Initializer(accountEmailAddress)
                   {
                       Scopes = new[] { AnalyticsService.Scope.AnalyticsReadonly }
                   }.FromCertificate(certificate));

                Service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credentials,
                    ApplicationName = "GAIntegration",
                });
            }

            public AnalyticDataPoint GetAnalyticsData(string profileId, string[] dimensions, string[] metrics, DateTime startDate, DateTime endDate)
            {
                AnalyticDataPoint data = new AnalyticDataPoint();
                if (!profileId.Contains("ga:"))
                    profileId = string.Format("ga:{0}", profileId);

                //Make initial call to service.
                //Then check if a next link exists in the response,
                //if so parse and call again using start index param.
                GaData response = null;
                do
                {
                    int startIndex = 1;
                    if (response != null && !string.IsNullOrEmpty(response.NextLink))
                    {
                        Uri uri = new Uri(response.NextLink);
                        var paramerters = uri.Query.Split('&');
                        string s = paramerters.First(i => i.Contains("start-index")).Split('=')[1];
                        startIndex = int.Parse(s);
                    }

                    var request = BuildAnalyticRequest(profileId, dimensions, metrics, startDate, endDate, startIndex);
                    response = request.Execute();
                    data.ColumnHeaders = response.ColumnHeaders;
                    data.Rows.AddRange(response.Rows);

                } while (!string.IsNullOrEmpty(response.NextLink));

                return data;
            }

            private DataResource.GaResource.GetRequest BuildAnalyticRequest(string profileId, string[] dimensions, string[] metrics,
                                                                                DateTime startDate, DateTime endDate, int startIndex)
            {
                DataResource.GaResource.GetRequest request = Service.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"),
                                                                                    endDate.ToString("yyyy-MM-dd"), string.Join(",", metrics));
                request.Dimensions = string.Join(",", dimensions);
                request.StartIndex = startIndex;
                return request;
            }

            public IList<Profile> GetAvailableProfiles()
            {
                var response = Service.Management.Profiles.List("~all", "~all").Execute();
                return response.Items;
            }

            public class AnalyticDataPoint
            {
                public AnalyticDataPoint()
                {
                    Rows = new List<IList<string>>();
                }

                public IList<GaData.ColumnHeadersData> ColumnHeaders { get; set; }
                public List<IList<string>> Rows { get; set; }
            }
        }

        public class ChartRecord     
        {     
            public ChartRecord(string date, int visits)     
            {     
                _date = date;     
                _visits = visits;     
            }     
            private string _date;     
            public string Date     
            {     
                get { return _date; }     
                set { _date = value; }     
            }     
            private int _visits;     
            public int Visits     
            {     
                get { return _visits; }     
                set { _visits = value; }     
            }     
        }    
    }
}
