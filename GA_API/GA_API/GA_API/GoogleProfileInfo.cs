﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace GA_API
{
    class GoogleProfileInfo
    {
        //client properties
        private String google_Analytics_ID__c;
        private String googleProfileID;
        private decimal unit_Level__c;
        private String dmsSourceID__c;
        private String area_ID__c;
        private String name;

        private GoogleProfileData gaProfileData;

        public GoogleProfileInfo()
        {
            //An instance of the Data access layer!
            gaProfileData = new GoogleProfileData();
        }

        public String Google_Analytics_ID__c
        {
            get
            {
                return this.google_Analytics_ID__c;
            }
            set
            {
                try
                {
                    this.google_Analytics_ID__c = value;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                }
            }
        }

        public String Google_Profile_ID__c
        {
            get
            {
                return this.googleProfileID;
            }
            set
            {
                try
                {
                    this.googleProfileID = value;

                    if (this.googleProfileID == "")
                    {
                        throw new Exception(
                            "Please provide valid Google Profile ID ...");
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                }
            }
        }

        public String DMSSourceID__c
        {

            get
            {
                return this.dmsSourceID__c;
            }
            set
            {
                try
                {
                    this.dmsSourceID__c = value;

                    if (this.dmsSourceID__c == "")
                    {
                        throw new Exception(
                            "Please provide DMS Source...");
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                }
            }
        }

        public String Area_ID__c
        {
            get
            {
                return this.area_ID__c;
            }
            set
            {
                //could be more checkings here eg revmove ' chars
                //change to proper case
                //blah blah
                this.area_ID__c = value;
                if (this.area_ID__c == "")
                {
                    throw new Exception("Please provide Org ID...");
                }

            }
        }

        public decimal Unit_Level__c
        {
            get
            {
                if (this.unit_Level__c == 0)
                {
                    return 1;
                }
                else
                {
                    return this.unit_Level__c;
                }
            }
            set
            {
                try
                {
                    this.unit_Level__c = value;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message.ToString());
                }
            }
        }

        public String Name
        {
            get
            {
                return this.name;
            }
            set
            {
                //could be more checkings here eg revmove ' chars
                //change to proper case
                //blah blah
                this.name = value;
                if (this.name == "")
                {
                    throw new Exception("Please provide Org Name...");
                }

            }
        }

        /// <SUMMARY>
        /// Function Find client. Calls the 
        /// function in Data layer.
        /// It returns the details of the client using 
        /// client ID via a Dataset to GUI tier.
        /// </SUMMARY>
        public DataSet GetGoogleProfiles()
        {
            DataSet data = null;

            try
            {
                data = gaProfileData.GetGoogleProfiles();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message.ToString());
            }

            return data;
        }
    }
}
